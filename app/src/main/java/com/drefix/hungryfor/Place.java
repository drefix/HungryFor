package com.drefix.hungryfor;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.common.collect.ComparisonChain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Date;

/**
 * Created by com.drefix on 10/23/15.
 */
public class Place {


    String id;
    String name;
    String phone;
    String address;
    String[] cuisineList;
    String website;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress(){
        return address;
    }

    public String[] getCuisineList(){
        return cuisineList;
    }

    public String getWebsite(){return website;}

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCuisineList(String[] cuisineList){
        this.cuisineList = cuisineList;
    }

    public void setWebsite(String website){this.website = website;}

    public ArrayList<Place> sortArray(ArrayList<Place> placeArrayList, String myPref){


        ArrayList<Place> returnArrayList = new ArrayList<Place>();


        String[] prefParts = new String[9];
        if (myPref != null) {
            prefParts = myPref.split(",");
        } else {
            for (int a = 0; a <= 8; a++) {
                prefParts[a] = "0";
            }
        }


        String[][] tempSortArray = new String[9][2];
        ArrayList<dreRateArray> rArray = new ArrayList<dreRateArray>();
        int sort[] = new int[9];
        StringBuilder tempSortString1 = new StringBuilder();
        StringBuilder tempSortString2 = new StringBuilder();
        for (int row = 0; row <= 8; row++) {
            dreRateArray tempRateArray = new dreRateArray(prefParts[row], Integer.toString(row));
            rArray.add(tempRateArray);
            //tempSortArray[row][0] = prefParts[row];
            //tempSortArray[row][1] = Integer.toString(row);
        }

        Collections.sort(rArray, new Comparator<dreRateArray>() {
            @Override
            public int compare(dreRateArray o1, dreRateArray o2) {
                return ComparisonChain.start().compare(Integer.valueOf(o2.a), Integer.valueOf(o1.a)).result();
            }
        });

        for (int y = 0; y < 8; y++) {
            // tempSortString1.append(tempSortArray[y][0] + ", ");

            tempSortString2.append(tempSortArray[y][1] + ", ");
        }

        // com.drefix -- a continuation of checking accuracy
        tempSortString1.delete(0, tempSortString1.length());
        tempSortString2.delete(0, tempSortString2.length());
        for (int y = 0; y <= 8; y++) {
            //tempSortString1.append(tempSortArray[y][0] + ", ");

            tempSortString2.append(tempSortArray[y][1] + ", ");
        }

//        Toast.makeText(getBaseContext(),tempSortString2.toString(), Toast.LENGTH_LONG).show();


        for (int x = 0; x <= 8; x++) {
            dreRateArray tempArray = rArray.get(x);
            sort[x] = tempArray.getB();

        }
        for (int x = 0; x <= 8; x++) {
            tempSortString1.append(sort[x]);
        }

        //Toast.makeText(getBaseContext(),tempSortString1.toString(), Toast.LENGTH_LONG).show();
        Log.e("sortString", tempSortString1.toString());

        ArrayList<rateList> resultArray = new ArrayList<rateList>();

        for (int i = 0; i < placeArrayList.size(); i++) {
            int parts[] = checkCuisine(placeArrayList.get(i).getCuisineList());

            rateList tempRate;

            tempRate = new rateList(parts[sort[0]], parts[sort[1]], parts[sort[2]], parts[sort[3]], parts[sort[4]], parts[sort[5]],
                    parts[sort[6]], parts[sort[7]], parts[sort[8]], i);

            resultArray.add(tempRate);
        }



        Collections.sort(resultArray, new Comparator<rateList>() {
            public int compare(rateList r2, rateList r1) {
                return ComparisonChain.start().compare(r1.col1, r2.col1).compare(r1.col2, r2.col2)
                        .compare(r1.col3, r2.col3).compare(r1.col4, r2.col4)
                        .compare(r1.col5, r2.col5).compare(r1.col6, r2.col6)
                        .compare(r1.col7, r2.col7).compare(r1.col8, r2.col8)
                        .compare(r1.col9, r2.col9).result();

            }
        });

        StringBuilder dre2 = new StringBuilder();


        for (int j = 0; j < resultArray.size(); j++) {
            rateList putList = resultArray.get(j);
            int position = putList.getPosition();
            returnArrayList.add(placeArrayList.get(position));
        }


        return returnArrayList;
    }

    public static class dreRateArray {
        String a, b;

        public dreRateArray(String a, String b) {
            this.a = a;
            this.b = b;
        }

        public int getB() {
            return Integer.valueOf(b);
        }
    }

    public static class rateList {

        /* com.drefix -- rethink how rateList(name?) is structured */

        int col1, col2, col3, col4, col5, col6, col7, col8, col9;
        int position;

        public rateList(int col1, int col2, int col3, int col4, int col5,
                        int col6, int col7, int col8, int col9,
                        int position) {
            this.col1 = col1;
            this.col2 = col2;
            this.col3 = col3;
            this.col4 = col4;
            this.col5 = col5;
            this.col6 = col6;
            this.col7 = col7;
            this.col8 = col8;
            this.col9 = col9;
            this.position = position;
        }


        public int getPosition() {
            return position;
        }

    }

    public static int[] checkCuisine(String[] cuisine) {

        String[] europeCuisine = {"Austrian", "Belgian", "British", "Calzones", "Czech", "EasternEuropean", "European", "French", "German", "Greek", "Portuguese", "Gyros", "Hungarian", "Italian", "Pizza", "Pasta", "Moroccan", "Mediterranean", "Norwegian"};
        List<String> europeList = Arrays.asList(europeCuisine);
        String[] midEastCuisine = {"Afghan", "Albanian", "Armenian", "Halal", "Middle Eastern", "Lebanese", "Pakistani"};
        List<String> midEastList = Arrays.asList(midEastCuisine);
        String[] americanCuisine = {"American", "Barbecue", "Burgers", "Cajun", "Californian", "Creole", "Soul Food", "Hawaiian"};
        List<String> americanList = Arrays.asList(americanCuisine);
        String[] asianCuisine = {"Asian", "Cambodian", "Chinese", "Dim Sum", "Japanese", "Korean", "Korean Barbeque", "Japanese", "Mongolian", "Pho", "Sushi", "Taiwanese", "Thai", "Tibetan", "Vietnamese"};
        List<String> asianList = Arrays.asList(asianCuisine);
        String[] southAmericanCuisine = {"Argentine", "Brazilian", "Chilean", "Latin", "Latin American", "Peruvian", "Salvadoran", "South American", "Venezuelan", "Colombian"};
        List<String> southAmericanList = Arrays.asList(southAmericanCuisine);
        String[] mexicanCuisine = {"Mexican", "Southwestern", "Tex Mex", "Tex/Mex"};
        List<String> mexicanList = Arrays.asList(mexicanCuisine);
        String[] indianCuisine = {"Bangladeshi", "Burmese", "Indian", "Malaysian"};
        List<String> indianList = Arrays.asList(indianCuisine);
        String[] bakeryCuisine = {"Donuts", "Bagels", "Bakery", "Pastries"};
        List<String> bakeryList = Arrays.asList(bakeryCuisine);
        String[] caribbeanCuisine = {"Caribbean", "Cuban", "Dominican", "Filipino", "Haitian", "Jamaican", "Puerto Rican"};
        List<String> caribbeanList = Arrays.asList(caribbeanCuisine);


        boolean[] hasCuisine = {false, false, false, false, false, false, false, false, false};

        int[] cuisineList = new int[9];

        for (int i = 0; i < cuisine.length; i++) {
            if (cuisine[i] != null) {
                if (europeList.contains(cuisine[i]) && !hasCuisine[0]) {
                    hasCuisine[0] = true;
                    //cuisineList.append("European, ");
                } else if (midEastList.contains(cuisine[i]) && !hasCuisine[1]) {
                    hasCuisine[1] = true;
                    //cuisineList.append("Mid East, ");
                } else if (americanList.contains(cuisine[i]) && !hasCuisine[2]) {
                    hasCuisine[2] = true;
                    //cuisineList.append("American, ");
                } else if (asianList.contains(cuisine[i]) && !hasCuisine[3]) {
                    hasCuisine[3] = true;
                    //cuisineList.append("Asian, ");
                } else if (southAmericanList.contains(cuisine[i]) && !hasCuisine[4]) {
                    hasCuisine[4] = true;
                    // cuisineList.append("South American, ");
                } else if (mexicanList.contains(cuisine[i]) && !hasCuisine[5]) {
                    hasCuisine[5] = true;
                    //cuisineList.append("Mexican, ");
                } else if (indianList.contains(cuisine[i]) && !hasCuisine[6]) {
                    hasCuisine[6] = true;
                    //cuisineList.append("Indian, ");
                } else if (bakeryList.contains(cuisine[i]) && !hasCuisine[7]) {
                    hasCuisine[7] = true;
                    //cuisineList.append("Bakery, ");
                } else if (caribbeanList.contains(cuisine[i]) && !hasCuisine[8]) {
                    hasCuisine[8] = true;
                    //cuisineList.append("Caribbean, ");
                } else {
                }

            }

        }
        for (int i = 0; i <= 8; i++) {
            if (hasCuisine[i]) {
                cuisineList[i] = 1;
            } else {
                cuisineList[i] = 0;
            }
        }

        return cuisineList;

    }

}
