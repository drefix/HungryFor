package com.drefix.hungryfor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.MainThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.factual.driver.Circle;
import com.factual.driver.Factual;
import com.factual.driver.Query;
import com.factual.driver.ReadResponse;
import com.google.common.collect.Lists;

import org.json.JSONArray;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DisplayList extends AppCompatActivity { //implements LocationListener {

    protected Factual factual = new Factual("Ph8w2ysYUIw6Dixk158o9oJpnMkwGRbAfkvDU54R", "PxBGxhM4yevZsTh6f2pM5sVUUSEgqvLdZgs3eQgG");
    private ListView displayList;
    private ArrayAdapter arrayAdapter;

    double latitude = 41.195631;
    double longitude = -96.1523;

    Context context;

    SwipeRefreshLayout swipeRefreshLayout;

    // String[] cuisineArray = new String [30];
    ArrayList<String[]> cuisineArrayList = new ArrayList<String[]>();
    String[] idArray; // = new String[toDisplay];
    int toDisplay;// = myPref.getInt("results", 30);
    int meters;// = milesToMeters(myPref.getInt("miles", 25));
    int whichDisplay = -1;
    String placeId;
    boolean showDetails = false;
    boolean random = false;

    private boolean mResult;
    Query query = null;
    Intent intent;

    String searchString;
    String title;

    SharedPreferences myPref;
    SharedPreferences.Editor editor;

    Set<String> favList = new HashSet<String>();
    Set<String> wishList = new HashSet<String>();

    Boolean faveNull = false;
    Boolean wishNull = false;


    int updateInterval = 500;
    LocationManager locationManager;
    LocationListener locationListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {


        intent = getIntent();

        whichDisplay = intent.getIntExtra("which_display", -1);
        searchString = intent.getStringExtra("search_string");

        switch (whichDisplay){
            case 0:
                title = "You might like...";
                break;
            case 1:
                fareDialog();
                break;
            case 2:
                searchDialog();
                break;
            case 3:
                title = "Your favorites";
                break;
            case 4:
                title = "Places you wanted to try";
                break;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_list);

        myPref = getSharedPreferences("hungryPref",0);
        editor = myPref.edit();

        if(myPref.getStringSet("favList",null)!= null){
            favList = myPref.getStringSet("favList", null);
        }
        else{
            faveNull = true;
        }

        if(myPref.getStringSet("wishList", null) != null){
            wishList = myPref.getStringSet("wishList", null);
        }
        else{
            wishNull = true;
        }


        ColorDrawable cd = new ColorDrawable();
        cd.setColor(0xff00a17d);

        setTitleColor(0xFF00FF00);
        setTitle(title);


        context = this;



        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        /*drefix - swipeRefreshLayout.setColorSchemeColors(R.color.actionbar_background); */
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });

        startActions();
    }

    private void refreshData(){

       /* locationManager = (LocationManager) getSystemService(
                Context.LOCATION_SERVICE);

        locationListener = this;

        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, updateInterval, 100, locationListener);

        ( new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
            }
        }, 3000);*/
        swipeRefreshLayout.setRefreshing(false);
        runTask();


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_display_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("About Hungry For");
            alert.setMessage("Created by: \n" +
                    "drefix \n\n" +
                    "Places data © Factual Inc. \n (https://www.factual.com)");
            alert.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                    dialog.dismiss();

                }
            });

            alert.show();

        }

        return super.onOptionsItemSelected(item);
    }

    public void startActions(){


        displayList = (ListView) findViewById(R.id.display_list);

        toDisplay  = 50; //= myPref.getInt("results", 50);
        meters = milesToMeters(10);
        idArray = new String[toDisplay];

        TextView displayText = (TextView) findViewById(R.id.display_text);

        if(faveNull && whichDisplay == 3){
           // displayList.setVisibility(View.INVISIBLE);
            displayText.setText("No favorites saved");
        }
        else if(wishNull && whichDisplay ==4){
           // displayList.setVisibility(View.INVISIBLE);
            displayText.setText("Nothing save in Wishlist");

        }
        else {
            displayText.setText("");
            runTask();
        }

    }

    public void searchDialog(){

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("What are you looking for?");
        alert.setMessage("Enter a name, style, or area of town");
// Set an EditText view to get user input
        final EditText input = new EditText(alert.getContext());
        input.setSingleLine();
        alert.setView(input);
        alert.setCancelable(false);
        alert.setPositiveButton("Search", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                searchString = input.getText().toString();
                title = "Results for " + input.getText().toString();
                //do what you want with your result

                dialog.dismiss();
                throw new RuntimeException();

            }
        });

        alert.show();

        // loop till a runtime exception is triggered.
        try { Looper.loop(); }
        catch(RuntimeException e2) {

        }
    }


    public void fareDialog() {
        // make a handler that throws a runtime exception when a message is received
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };

        final String[] items  = {"American",
                "BBQ",
                "Chinese",
                "French",
                "Greek",
                "Indian",
                "Irish",
                "Italian",
                "Indian",
                "Mexican",
                "Pizza",
                "Seafood",
                "Soul Food",
                "Spanish",
                "Tex-Mex",
                "Thai"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose your fare:");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                searchString = items[item];
                title = "Nearby " + items[item] + " places";

                dialog.dismiss();
                throw new RuntimeException();

            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(false);

        alert.show();


        // loop till a runtime exception is triggered.
        try { Looper.loop(); }
        catch(RuntimeException e2) {

        }
    }


    public void runTask() {


        FactualRetrievalTask task = new FactualRetrievalTask();

        switch (whichDisplay) {
            case 0:
                query = new Query()
                    .within(new Circle(MainActivity.latitude, MainActivity.longitude, meters))
                    .limit(toDisplay)
                    .sortAsc("$distance");
                //.only("name", "address", "tel");
                break;
            case 1:
            case 2:
                //search

                query = new Query().within(new Circle(MainActivity.latitude, MainActivity.longitude, 25000)).search(searchString);
                //.only("name", "address", "tel");
                break;

            case 3:

                if(favList != null) {
                    StringBuilder favString = new StringBuilder();
                    String[] favArray = favList.toArray(new String[favList.size()]);
                    for (int i = 0; i < favArray.length; i++) {
                        if (i != (favArray.length - 1)) {
                            favString.append(favArray[i] + ", ");
                        } else {
                            favString.append(favArray[i]);
                        }

                    }
                    query = new Query().search(favString.toString());
                }

                break;

            case 4:

                if(wishList != null) {
                    StringBuilder wishString = new StringBuilder();
                    String[] wishArray = wishList.toArray(new String[wishList.size()]);
                    for (int i = 0; i < wishArray.length; i++) {
                        if (i != (wishArray.length - 1)) {
                            wishString.append(wishArray[i] + ", ");
                        } else {
                            wishString.append(wishArray[i]);
                        }

                    }

                    query = new Query().search(wishString.toString());
                }

                break;

            case -1:
                Toast.makeText(getBaseContext(), "Hit Default", Toast.LENGTH_SHORT).show();
                return;
        }


        task.execute(query);

    }

   /*   @Override
    public void onLocationChanged(Location location) {

        MainActivity.latitude = location.getLatitude();
        MainActivity.longitude = location.getLongitude();
        MainActivity.locationFound = true;
        locationManager.removeUpdates(locationListener);

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
*/
    protected class FactualRetrievalTask extends AsyncTask<Query, Integer, List<ReadResponse>> {



        @Override

        protected List<ReadResponse> doInBackground(Query... params) {
            List<ReadResponse> results = Lists.newArrayList();
            for (Query q : params) {
                results.add(factual.fetch("restaurants-us", q));
            }
            return results;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected void onPostExecute(final List<ReadResponse> responses) {
            Log.d("drefixResponse", responses.toString());

            ArrayList<Place> places = new ArrayList<Place>();

                StringBuffer sb = new StringBuffer();
                String[] placeArray = new String[toDisplay];
                for (int d = 0; d < toDisplay; d++) {
                    placeArray[d] = "";
                }
                int arrayLoc = 0;

                for (ReadResponse response : responses) {
                    for (Map<String, Object> restaurant : response.getData()) {
                        Place place = new Place();
                        String name = (String) restaurant.get("name");
                        place.setName((String) restaurant.get("name"));

                        String address = (String) restaurant.get("address");
                        place.setAddress((String) restaurant.get("address"));

                        String phone = (String) restaurant.get("tel");
                        place.setPhone((String) restaurant.get("tel"));

                        String factId = (String) restaurant.get("factual_id");
                        place.setId((String) restaurant.get("factual_id"));

                        String website = (String) restaurant.get("website");
                        place.setWebsite((String) restaurant.get("website"));

                        Number distance = (Number) restaurant.get("$distance");
                        idArray[arrayLoc] = factId;
                        //if (whichDisplay == 0 || MainActivity.useZip || whichDisplay == 2 || whichDisplay == 3) {
                        sb.append(name);
                        /*} else {
                            sb.append(name + " -- " + metersToMiles(distance) + "miles");
                            // sb.append(name);
                        }*/
                        placeArray[arrayLoc] = sb.toString();

                        arrayLoc++;
                        sb.delete(0, sb.length());
                        String[] cuisineArray = new String[toDisplay];
                        if (restaurant.get("cuisine") != null) {
                            JSONArray cuisine = (JSONArray) restaurant.get("cuisine");
                            cuisineArray = new String[cuisine.length()];

                            for (int i = 0; i < cuisine.length(); i++) {

                                try {
                                    cuisineArray[i] = cuisine.getString(i);

                                } catch (Exception e) {
                                    Log.e("dreError", e.getMessage());
                                }
                            }

                        } else {
                            cuisineArray[0] = "No Cuisine";
                        }
                        cuisineArrayList.add(cuisineArray);
                        place.setCuisineList(cuisineArray);
                        places.add(place);
                    }
                }

                    if(places.size() == 0){

                        refreshData();

                    }
                    else {
                        places = new Place().sortArray(places, MainActivity.prefString);

                        displayList.setBackgroundColor(0X00000000);
                        displayList.setDivider(null);

                        //displayList.setAdapter(arrayAdapter);
                        displayList.setAdapter(new CardAdapter(context, places));

                    }
        }

    }

    public Number metersToMiles(Number meters) {

        double miles;
        miles = meters.doubleValue() * 0.000621371;
        DecimalFormat df = new DecimalFormat("#.#");
        miles = Double.valueOf(df.format(miles));

        return miles;
    }


    public int milesToMeters(int miles) {

        if (miles == 10) {
            return 16000;
        } else if (miles == 15) {
            return 20000;
        } else {
            return 8000;
        }

    }







}

