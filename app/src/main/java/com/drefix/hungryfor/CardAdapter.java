package com.drefix.hungryfor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by com.drefix on 10/23/15.
 */
public class CardAdapter extends BaseAdapter
{

    Set<String> favList = new HashSet<String>();
    Set<String> wishList = new HashSet<String>();


    ArrayList<Place> places;
    Context context;
    private static LayoutInflater inflater=null;
    public CardAdapter(Context context, ArrayList<Place> places) {
        // TODO Auto-generated constructor stub
        this.places = places;
        this.context=context;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return places.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView name;
        TextView phone;
        TextView address;
        ImageView img;
    }
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        Holder holder=new Holder();
        View rowView = null;
        rowView = inflater.inflate(R.layout.cards, null);

        holder.name=(TextView) rowView.findViewById(R.id.card_name);
        holder.name.setText(places.get(position).getName());

        holder.phone=(TextView) rowView.findViewById(R.id.card_phone);
        holder.phone.setText(places.get(position).getPhone());

        holder.address=(TextView) rowView.findViewById(R.id.card_address);
        holder.address.setText(places.get(position).getAddress());

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final SharedPreferences myPref= context.getSharedPreferences("hungryPref",0);;
                final SharedPreferences.Editor editor = myPref.edit();


                final String[] items = {
                        "Visit Website" ,
                        "Call " + places.get(position).getName(),
                        "Go to " + places.get(position).getAddress(),
                        "Add " + places.get(position).getName() + " to Fave",
                        "Add to " + places.get(position).getName() + " Wish"};

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(places.get(position).getName());
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item) {
                            case 0:
                                String url = places.get(position).getWebsite();
                                if (url != null){
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(places.get(position).getWebsite()));
                                    context.startActivity(i);
                                }
                                else{
                                    Toast.makeText(context,"Website not Available", Toast.LENGTH_LONG).show();
                                }

                                break;

                            case 1:

                                StringBuilder phone = new StringBuilder();
                                phone.append("tel:" + places.get(position).getPhone());

                                Intent phoneIntent = new Intent(Intent.ACTION_DIAL);
                                phoneIntent.setData(Uri.parse(phone.toString()));
                                context.startActivity(phoneIntent);
                                break;

                            case 2:
                                Intent mapIntent = new Intent(Intent.ACTION_VIEW);

                                mapIntent.setData(Uri.parse("google.navigation:q=" + places.get(position).getAddress()));
                                context.startActivity(mapIntent);
                                break;
                            case 3:

                                if (myPref.getStringSet("favList", null) != null) {
                                    favList = myPref.getStringSet("favList",null);
                                }
                                favList.add(places.get(position).getId());
                                editor.putStringSet("favList", favList);
                                editor.commit();

                                break;


                            case 4:
                                if (myPref.getStringSet("wishList", null) != null) {
                                    wishList = myPref.getStringSet("wishList", null);
                                }
                                wishList.add(places.get(position).getId());
                                editor.putStringSet("wishList", wishList);
                                editor.commit();

                                Toast.makeText(context, "Added " + places.get(position).getName() + " to Wish List", Toast.LENGTH_LONG).show();
                                break;
                        }
                        dialog.dismiss();

                    }
                });
                AlertDialog alert = builder.create();

                alert.show();
            }
        });

        Button like = (Button) rowView.findViewById(R.id.card_like);
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                final SharedPreferences myPref= context.getSharedPreferences("hungryPref",0);;
                final SharedPreferences.Editor editor = myPref.edit();

                String prefArray = "";

                String[] prefParts = new String[9];

                if (MainActivity.prefString != null) {
                    prefArray = MainActivity.prefString;
                    prefParts = prefArray.split(",");
                } else {
                    for (int i = 0; i <= 8; i++) {
                        prefParts[i] = "0";
                    }
                }

                StringBuilder dreSB = new StringBuilder();
                //int[] intParts = new int[parts.length];
                int[] intPrefParts = new int[prefParts.length];


                int[] temp = Place.checkCuisine(places.get(position).getCuisineList());


                for (int j = 0; j <= 8; j++) {

                    intPrefParts[j] = Integer.valueOf(prefParts[j]);
                    if (intPrefParts[j] < 8) {
                        intPrefParts[j] += Integer.valueOf(temp[j]);
                    }
                    if (j <= 7) {
                        dreSB.append(intPrefParts[j] + ",");
                    } else {
                        dreSB.append(intPrefParts[j]);
                    }
                }
                MainActivity.prefString = dreSB.toString();
                myPref.edit().putString("prefArray", dreSB.toString());
                myPref.edit().commit();

                Toast.makeText(context, "Yay!", Toast.LENGTH_SHORT).show();

            }
        });

        Button dislike = (Button) rowView.findViewById(R.id.card_dislike);
        dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final SharedPreferences myPref= context.getSharedPreferences("hungryPref",0);;
                final SharedPreferences.Editor editor = myPref.edit();

                String prefArray = "";

                String[] prefParts = new String[9];

                if (MainActivity.prefString != null) {
                    prefArray = MainActivity.prefString;
                    prefParts = prefArray.split(",");
                } else {
                    for (int i = 0; i <= 8; i++) {
                        prefParts[i] = "0";
                    }
                }

                StringBuilder dreSB = new StringBuilder();
                //int[] intParts = new int[parts.length];
                int[] intPrefParts = new int[prefParts.length];


                int[] temp = Place.checkCuisine(places.get(position).getCuisineList());

                for (int j = 0; j <= 8; j++) {

                    intPrefParts[j] = Integer.valueOf(prefParts[j]);
                    if (intPrefParts[j] > -8) {
                        intPrefParts[j] -= Integer.valueOf(temp[j]);
                    }

                    if (j <= 7) {
                        dreSB.append(intPrefParts[j] + ",");
                    } else {
                        dreSB.append(intPrefParts[j]);
                    }
                }
                MainActivity.prefString = dreSB.toString();
                myPref.edit().putString("prefArray", dreSB.toString());
                myPref.edit().commit();

                Toast.makeText(context, "Eww!", Toast.LENGTH_SHORT).show();


            }
        });

        return rowView;
    }

}
